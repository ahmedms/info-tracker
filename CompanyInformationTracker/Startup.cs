﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CompanyInformationTracker.Startup))]
namespace CompanyInformationTracker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

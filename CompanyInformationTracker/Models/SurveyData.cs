﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_SURVEY_DATA")]
    public class SurveyData
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("SURVEY_ID")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Survey date")]
        [Column("SURVEY_DATE", TypeName = "DateTime2")]
        public DateTime SurveyDate { get; set; }

        /*[Required]
        [Column("LINK_GEN_DATE", TypeName = "DateTime2")]
        public DateTime LinkGenDate { get; set; }*/

        [Required]
        [Column("UPDATE_DATE", TypeName = "DateTime2")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "Full time employees")]
        [Column("FULL_TIME_EMPLOYEES")]
        public long FullTimeEmployees { get; set; }

        [Required]
        [Display(Name = "Part time employees")]
        [Column("PART_TIME_EMPLOYEES")]
        public long PartTimeEmployees { get; set; }

        [Required]
        [Display(Name = "Gross monthly salary")]
        [Column("GROSS_MONTHLY_SALARY")]
        [DataType("decimal(18,5)")]
        public Decimal GrossMonthlySalary { get; set; }

        [Required]
        [Display(Name = "Gross revenue")]
        [Column("GROSS_REVENUE")]
        [DataType("decimal(18,5)")]
        public Decimal GrossRevenue{ get; set; }

        [Display(Name = "Undergoing merger, acquisition or closure in the next six months?")]
        [Column("UNDERGO_MERGE")]
        [DataType("bit")]
        public bool UngoMerge { get; set; }

        [Required]
        [Column("IS_BASE_DATA", TypeName = "bit")]
        public bool IsBaseData { get; set; }

        [ForeignKey("MEMBER_ID")]
        public virtual Member Member { get; set; }
        public long MEMBER_ID { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_REFERRER")]
    public class Referrer
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("REFERRER_ID")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Referrer Name")]
        [MaxLength(50)]
        [Column("SOURCE_NAME", TypeName = "varchar")]
        [Index(IsUnique = true)]
        public string SourceName { get; set; }
    }
}
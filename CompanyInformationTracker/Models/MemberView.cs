﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyInformationTracker.Models
{
    public class MemberView
    {
        public long MEMBER_ID { get; set; }
        public string MEMBER_NAME { get; set; }
        public DateTime DATE_INCORP { get; set; }
        public DateTime DATE_ENTERED { get; set; }
        public string NAICS_CODE { get; set; }
        public string WEBSITE { get; set; }
        public string DESCRIPTION { get; set; }
        public string STREET_NO { get; set; }
        public string ADDITIONAL { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string POSTAL_CODE { get; set; }
        public string COUNTRY { get; set; }
        public string NAME { get; set; } // legal entity type name
        public string TYPE_NAME { get; set; } //internal member type name
        public string SOURCE_NAME { get; set; } //referre name
        public string DEMOGRAPHY_NAME { get; set; }
        public string STATUS_NAME { get; set; }
        public DateTime STATUS_DATE { get; set; }
        
    }
}
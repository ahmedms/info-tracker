﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_DEMOGRAPY_INFO")]
    public class DemographyInfo
    {
        [Column(Order = 1), Key, ForeignKey("Member")]
        public long MEMBER_ID { get; set; }
        public Member Member { get; set; }

        [Column(Order = 2), Key, ForeignKey("MemberDemography")]
        public long DEMOGRAPHY_ID { get; set; }
        public virtual MemberDemography MemberDemography { get; set; }

        [Display(Name = "Member Demogrphy")]
        public IEnumerable<MemberDemography> MemberDemographyList { get; set; }
    }
}
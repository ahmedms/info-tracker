﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_EMPLOYEE")]
    public class Employee
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("EMP_ID")]
        public long Id { get; set; }

        [Required]
        [MaxLength(48)]
        [Column("LAST_NAME", TypeName = "varchar")]
        public string LastName { get; set; }

        [Required]
        [MaxLength(48)]
        [Column("FIRST_NAME", TypeName = "varchar")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("TITLE", TypeName = "varchar")]
        public string Title { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("PHONE_TYPE", TypeName = "varchar")]
        public string PhoneType { get; set; }

        [Required]
        [MaxLength(20)]
        [Column("PHONE_NUMBER", TypeName = "varchar")]
        public string PhoneNumber { get; set; }

        [Required]
        [MaxLength(50)]
        //[Index(IsUnique = true)] //Disabling for not having much email address at this moment
        [Column("EMAIL", TypeName = "varchar")]
        public string Email { get; set; }

        [Required]
        [Column("IS_PRIMARY", TypeName = "bit")]
        public bool IsPrimary { get; set; }

        [ForeignKey("MEMBER_ID")]
        public virtual Member Member { get; set; }
        public long MEMBER_ID { get; set; }
    }
}
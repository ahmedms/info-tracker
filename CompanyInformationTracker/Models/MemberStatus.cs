﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_MEMBER_STATUS")]
    public class MemberStatus
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("STATUS_ID")]
        public long Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("STATUS_NAME", TypeName = "varchar")]
        public string StatusName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{

    public class SurveyDBModel
    {
        public long MemberId { get; set; }
        public DateTime SurveyYear { get; set; }
        public long ftEmplyees { get; set; }
        public long ptEmployees { get; set; }
        public Decimal gross { get; set; }
        public Decimal revenue { get; set; }
        public bool isMerge { get; set; }
        public bool isBase { get; set; }
        public Decimal GrantAmout { get; set; }
        public Decimal DebtAmount { get; set; }
        public Decimal EquityAmount { get; set; }
    }

    public class SurveyDBModelExtended
    {
        public long MemberId { get; set; }
        public string Header { get; set; }
        public DateTime SurveyYear { get; set; }
        public long numberOfMember { get; set; }
        public long ftEmplyees { get; set; }
        public long ptEmployees { get; set; }
        public Decimal gross { get; set; }
        public Decimal revenue { get; set; }
        public bool isMerge { get; set; }
        public bool isBase { get; set; }
        public Decimal GrantAmout { get; set; }
        public Decimal DebtAmount { get; set; }
        public Decimal EquityAmount { get; set; }
    }
}
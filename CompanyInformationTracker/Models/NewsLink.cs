﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_NEWS_LINK")]
    public class NewsLink
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("LINK_ID")]
        public long Id { get; set; }

        [Required]
        [MaxLength(255)]
        [Column("LINK", TypeName = "varchar")]
        public string Link { get; set; }

        [ForeignKey("MEMBER_ID")]
        public virtual Member Member { get; set; }
        public long MEMBER_ID { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    public class MemberViewModel
    {
        public Member Member { get; set; }
        public Employee Employee { get; set; }
        public DemographyInfo DemographyInfo { get; set; }
        public StatusHistory StatusHistory { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_EQUITY")]
    public class Equity
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("EQUITY_ID")]
        public long Id { get; set; }

        [Required]
        [MaxLength(255)]
        [Column("SOURCE", TypeName = "varchar")]
        public string Source { get; set; }

        [Required]
        [Column("AMOUNT")]
        public double Amount { get; set; }

        [ForeignKey("SURVEY_ID")]
        public virtual SurveyData SurveyData { get; set; }
        public long SURVEY_ID { get; set; }
    }
}
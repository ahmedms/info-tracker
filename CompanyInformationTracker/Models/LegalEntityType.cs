﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_LEGAL_ENTITY_TYPE")]
    public class LegalEntityType
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("LEGAL_ENTITY_ID")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Type Name")]
        [MaxLength(50)]
        [Column("NAME", TypeName = "varchar")]
        [Index(IsUnique = true)]
        public string TypeName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_MEMBER_DEMOGRAPHY")]
    public class MemberDemography
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("DEMOGRAPHY_ID")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Demography Name")]
        [MaxLength(50)]
        [Column("DEMOGRAPHY_NAME", TypeName = "varchar")]
        [Index(IsUnique = true)]
        public string DemographyName { get; set; }
    }
}
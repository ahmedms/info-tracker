﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CompanyInformationTracker.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false)
        {

        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Debt> Debts { get; set; }
        public DbSet<DemographyInfo> DemographyInfo { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Equity> Equities { get; set; }
        public DbSet<Grant> Grants { get; set; }
        public DbSet<InternalMemberType> InternalMemberTypes { get; set; }
        public DbSet<LegalEntityType> LegalEntityTypes { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<MemberDemography> MemberDemographies { get; set; }
        public DbSet<MemberStatus> MemberStatuses { get; set; }
        public DbSet<NewsLink> NewsLinks { get; set; }
        public DbSet<Referrer> Referrers { get; set; }
        public DbSet<StatusHistory> StatusHistories { get; set; }
        public DbSet<SurveyData> SurveyData { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Attendee> Attendees { get; set; }
    }
}
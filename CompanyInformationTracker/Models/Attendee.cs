﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_ATTENDEE")]
    public class Attendee
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ATTENDEE_ID")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        [MaxLength(255)]
        [Column("NAME", TypeName = "varchar")]
        public string Name { get; set; }

        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Column("EMAIL", TypeName = "varchar")]
        public string Email { get; set; }

        [ForeignKey("EVENT_ID")]
        public virtual Event Event { get; set; }
        public long EVENT_ID { get; set; }
    }
}
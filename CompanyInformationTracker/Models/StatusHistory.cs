﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_STATUS_HISTORY")]
    public class StatusHistory
    {
        [Column(Order=1), Key, ForeignKey("Member")]
        public long MEMBER_ID { get; set; }
        public Member Member { get; set; }

        [Column(Order = 2), Key, ForeignKey("MemberStatus")]
        public long STATUS_ID { get; set; }
        public virtual MemberStatus MemberStatus { get; set; }

        [Display(Name = "Member Status")]
        public IEnumerable<MemberStatus> MemberStatusList { get; set; }

        [Required]
        [Column("STATUS_DATE", TypeName = "DateTime2")]
        public DateTime StatusDate { get; set; }
    }
}
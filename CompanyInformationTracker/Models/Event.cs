﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_EVENT")]
    public class Event
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("EVENT_ID")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Event Type")] //Since We are not sure about the type, we keep type in event table
        [MaxLength(255)]
        [Column("EVENT_TYPE", TypeName = "varchar")]
        public string EventType { get; set; }

        [Required]
        [Display(Name = "Event Title")]
        [MaxLength(255)]
        [Column("EVENT_TITLE", TypeName = "varchar")]
        public string EventTitle { get; set; }

        [Required]
        [Display(Name = "Location")]
        [MaxLength(255)]
        [Column("LOCATION", TypeName = "varchar")]
        public string Location { get; set; }

        [Display(Name = "Event Duration")]
        [Column("EVENT_DURATION")]
        public long EventDuration { get; set; }

        [Display(Name = "Number of Attendees")]
        [Column("ATTENDEE")]
        public long NumOfAttendee { get; set; }

        [Display(Name = "Event Description")]
        [Column("DESCRIPTION", TypeName = "varchar")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Event Date")]
        [Column("EVENT_DATE", TypeName = "DateTime2")]
        public DateTime EventDate { get; set; }
        
        [Required]
        [Display(Name = "Creation Date")]
        [Column("CREATION_DATE", TypeName = "DateTime2")]
        public DateTime CreationDate { get; set; }
        
        [Required]
        [Display(Name = "Update Date")]
        [Column("UPDATE_DATE", TypeName = "DateTime2")]
        public DateTime UpdateDate { get; set; }
        
        [Required]
        [Display(Name = "Event Creator")]
        [MaxLength(255)]
        [Column("EVENT_CREATOR", TypeName = "varchar")]
        public string EventCreator { get; set; }
        
        [Required]
        [Display(Name = "Event Status")]  //Since we are not sure about how many statuses, we keep this in event table
        [Column("EVENT_STATUS", TypeName = "varchar")] //two default status = pending, complete
        public string EventStatus { get; set; }

        
    }
}
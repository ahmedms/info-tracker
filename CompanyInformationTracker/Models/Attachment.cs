﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_ATTACHMENT")]
    public class Attachment
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ATTACHMENT_ID")]
        public long Id { get; set; }

        [Required]
        [MaxLength(255)]
        [Column("ATTACHMENT_NAME", TypeName = "varchar")]
        public string AttachmentName { get; set; }

        [Required]
        [MaxLength(255)]
        [Column("PATH", TypeName = "varchar")]
        public string Path { get; set; }

        [Required]
        [Column("DATE", TypeName = "DateTime2")]
        public DateTime Date { get; set; }

        
        [ForeignKey("MEMBER_ID")]
        public virtual Member Member { get; set; }
        public long MEMBER_ID { get; set; }

    }
}
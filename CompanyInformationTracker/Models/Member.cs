﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CompanyInformationTracker.Models
{
    [Table("tbl_MEMBER")]
    public class Member
    {
        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("MEMBER_ID")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Member Name")]
        [MaxLength(255)]
        [Index("Idx_member_name")]
        [Column("MEMBER_NAME", TypeName = "varchar")]
        public string MemberName { get; set; }

        [Required]
        [Display(Name = "Date Incorporated")]
        [Column("DATE_INCORP", TypeName = "DateTime2")]
        public DateTime DateIncorp { get; set; }

        [Display(Name = "Date Entered Incubator")]
        [Column("DATE_ENTERED", TypeName = "DateTime2")]
        public DateTime DateEntered { get; set; }

        [Required]
        [Display(Name = "NAICS Code")]
        [MaxLength(10)]
        [Column("NAICS_CODE", TypeName = "varchar")]
        public string NaicsCode { get; set; }

        /*[Required]
        [Column("IS_GRADUATED", TypeName = "bit")]
        public bool IsGraduated { get; set; }*/

        [Required]
        [Display(Name = "Street")]
        [MaxLength(35)]
        [Column("STREET_NO", TypeName = "varchar")]
        public string StreetNo { get; set; }

        [Display(Name = "Other")]
        [MaxLength(35)]
        [Column("ADDITIONAL", TypeName = "varchar")]
        public string AdditionalLine { get; set; }

        [Required]
        [MaxLength(30)]
        [Column("CITY", TypeName = "varchar")]
        public string City { get; set; }

        [Required]
        [MaxLength(30)]
        [Column("STATE", TypeName = "varchar")]
        public string State { get; set; }

        [Required]
        [MaxLength(9)]
        [Display(Name = "ZIP")]
        [Column("POSTAL_CODE", TypeName = "varchar")]
        public string PostalCode { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("COUNTRY", TypeName = "varchar")]
        public string Country { get; set; }

        /*[Required]
        [Display(Name = "Current Status")]
        [MaxLength(10)]
        [Column("CUR_STATUS", TypeName = "varchar")]
        public string CurStatus { get; set; }

        [Required]
        [Display(Name = "Current Status Date")]
        [Column("CUR_STATUS_DATE", TypeName = "DateTime2")]
        public DateTime CurStatusDate { get; set; }
        */

        /*[ForeignKey("ADDRESS_ID")]
        public virtual Address Address { get; set; }
        public long ADDRESS_ID { get; set; }*/

        [MaxLength(255)]
        [Column("WEBSITE", TypeName = "varchar")]
        public string Website { get; set; }

        [MaxLength(255)]
        [Column("DESCRIPTION", TypeName = "varchar")]
        public string Description { get; set; }

        [ForeignKey("LEGAL_ENTITY_ID")]
        public virtual LegalEntityType LegalEntityType { get; set; }
        public long LEGAL_ENTITY_ID { get; set; }

        [Display(Name = "Legal Entity")]
        public IEnumerable<LegalEntityType> LegalEntityTypeList { get; set; }


        //By default, the following statemetns create an index for TYPE_ID
        [ForeignKey("TYPE_ID")]
        public virtual InternalMemberType InternalMemberType { get; set; }
        public long TYPE_ID { get; set; }
        [Display(Name = "Member Type")]
        public IEnumerable<InternalMemberType> InternalTypeList { get; set; }



        /*[ForeignKey("DEMOGRAPHY_ID")]
        public virtual MemberDemography MemberDemography { get; set; }
        public long DEMOGRAPHY_ID { get; set; }
        [Display(Name = "Member Demography")]
        public IEnumerable<SelectListItem> MemberDemographyList { get; set; }*/


        [ForeignKey("REFERRER_ID")]
        public virtual Referrer Referrer { get; set; }
        public long REFERRER_ID { get; set; }

        [Display(Name = "Referrer")]
        public IEnumerable<Referrer> ReferrerList { get; set; }

        [Required]
        [Column("IS_ACTIVE", TypeName = "bit")]
        public bool IsActive { get; set; }

        [Required]
        [Column("LAST_UPDATE", TypeName = "DateTime2")]
        public DateTime LastUpdate { get; set; }
    }
}
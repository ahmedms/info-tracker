namespace CompanyInformationTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewAddressLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tbl_ADDRESS", "ADDITIONAL", c => c.String(nullable: false, maxLength: 35, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.tbl_ADDRESS", "ADDITIONAL");
        }
    }
}

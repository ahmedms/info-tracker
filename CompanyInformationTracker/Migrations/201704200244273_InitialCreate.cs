namespace CompanyInformationTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_ADDRESS",
                c => new
                    {
                        ADDRESS_ID = c.Long(nullable: false, identity: true),
                        STREET_NO = c.String(nullable: false, maxLength: 35, unicode: false),
                        CITY = c.String(nullable: false, maxLength: 30, unicode: false),
                        STATE = c.String(nullable: false, maxLength: 30, unicode: false),
                        POSTAL_CODE = c.String(nullable: false, maxLength: 9, unicode: false),
                        COUNTRY = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ADDRESS_ID);
            
            CreateTable(
                "dbo.tbl_ATTACHMENT",
                c => new
                    {
                        ATTACHMENT_ID = c.Long(nullable: false, identity: true),
                        ATTACHMENT_NAME = c.String(nullable: false, maxLength: 255, unicode: false),
                        PATH = c.String(nullable: false, maxLength: 255, unicode: false),
                        DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        MEMBER_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ATTACHMENT_ID)
                .ForeignKey("dbo.tbl_MEMBER", t => t.MEMBER_ID, cascadeDelete: true)
                .Index(t => t.MEMBER_ID);
            
            CreateTable(
                "dbo.tbl_MEMBER",
                c => new
                    {
                        MEMBER_ID = c.Long(nullable: false, identity: true),
                        MEMBER_NAME = c.String(nullable: false, maxLength: 255, unicode: false),
                        DATE_INCORP = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        NAICS_CODE = c.String(nullable: false, maxLength: 10, unicode: false),
                        DESCRIPTION = c.String(nullable: false, maxLength: 255, unicode: false),
                        WEBSITE = c.String(maxLength: 255, unicode: false),
                        LEGAL_ENTITY_ID = c.Long(nullable: false),
                        TYPE_ID = c.Long(nullable: false),
                        ADDRESS_ID = c.Long(nullable: false),
                        REFERRER_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.MEMBER_ID)
                .ForeignKey("dbo.tbl_ADDRESS", t => t.ADDRESS_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_INTERNAL_MEMBER_TYPE", t => t.TYPE_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_LEGAL_ENTITY_TYPE", t => t.LEGAL_ENTITY_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_REFERRER", t => t.REFERRER_ID, cascadeDelete: true)
                .Index(t => t.MEMBER_NAME, name: "Idx_member_name")
                .Index(t => t.LEGAL_ENTITY_ID)
                .Index(t => t.TYPE_ID)
                .Index(t => t.ADDRESS_ID)
                .Index(t => t.REFERRER_ID);
            
            CreateTable(
                "dbo.tbl_INTERNAL_MEMBER_TYPE",
                c => new
                    {
                        TYPE_ID = c.Long(nullable: false, identity: true),
                        TYPE_NAME = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.TYPE_ID)
                .Index(t => t.TYPE_NAME, unique: true);
            
            CreateTable(
                "dbo.tbl_LEGAL_ENTITY_TYPE",
                c => new
                    {
                        LEGAL_ENTITY_ID = c.Long(nullable: false, identity: true),
                        NAME = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.LEGAL_ENTITY_ID)
                .Index(t => t.NAME, unique: true);
            
            CreateTable(
                "dbo.tbl_REFERRER",
                c => new
                    {
                        REFERRER_ID = c.Long(nullable: false, identity: true),
                        SOURCE_NAME = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.REFERRER_ID)
                .Index(t => t.SOURCE_NAME, unique: true);
            
            CreateTable(
                "dbo.tbl_ATTENDEE",
                c => new
                    {
                        ATTENDEE_ID = c.Long(nullable: false, identity: true),
                        NAME = c.String(nullable: false, maxLength: 255, unicode: false),
                        EMAIL = c.String(maxLength: 8000, unicode: false),
                        EVENT_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ATTENDEE_ID)
                .ForeignKey("dbo.tbl_EVENT", t => t.EVENT_ID, cascadeDelete: true)
                .Index(t => t.EVENT_ID);
            
            CreateTable(
                "dbo.tbl_EVENT",
                c => new
                    {
                        EVENT_ID = c.Long(nullable: false, identity: true),
                        EVENT_TYPE = c.String(nullable: false, maxLength: 255, unicode: false),
                        EVENT_TITLE = c.String(nullable: false, maxLength: 255, unicode: false),
                        LOCATION = c.String(nullable: false, maxLength: 255, unicode: false),
                        EVENT_DURATION = c.Long(nullable: false),
                        ATTENDEE = c.Long(nullable: false),
                        DESCRIPTION = c.String(maxLength: 8000, unicode: false),
                        EVENT_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CREATION_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UPDATE_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EVENT_CREATOR = c.String(nullable: false, maxLength: 255, unicode: false),
                        EVENT_STATUS = c.String(nullable: false, maxLength: 8000, unicode: false),
                    })
                .PrimaryKey(t => t.EVENT_ID);
            
            CreateTable(
                "dbo.tbl_DEBT",
                c => new
                    {
                        DEBT_ID = c.Long(nullable: false, identity: true),
                        SOURCE = c.String(nullable: false, maxLength: 255, unicode: false),
                        AMOUNT = c.Double(nullable: false),
                        SURVEY_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.DEBT_ID)
                .ForeignKey("dbo.tbl_SURVEY_DATA", t => t.SURVEY_ID, cascadeDelete: true)
                .Index(t => t.SURVEY_ID);
            
            CreateTable(
                "dbo.tbl_SURVEY_DATA",
                c => new
                    {
                        SURVEY_ID = c.Long(nullable: false, identity: true),
                        SURVEY_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LINK_GEN_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        UPDATE_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        FULL_TIME_EMPLOYEES = c.Long(nullable: false),
                        PART_TIME_EMPLOYEES = c.Long(nullable: false),
                        GROSS_MONTHLY_SALARY = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GROSS_REVENUE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MEMBER_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.SURVEY_ID)
                .ForeignKey("dbo.tbl_MEMBER", t => t.MEMBER_ID, cascadeDelete: true)
                .Index(t => t.MEMBER_ID);
            
            CreateTable(
                "dbo.tbl_DEMOGRAPY_INFO",
                c => new
                    {
                        MEMBER_ID = c.Long(nullable: false),
                        DEMOGRAPHY_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.MEMBER_ID, t.DEMOGRAPHY_ID })
                .ForeignKey("dbo.tbl_MEMBER", t => t.MEMBER_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_MEMBER_DEMOGRAPHY", t => t.DEMOGRAPHY_ID, cascadeDelete: true)
                .Index(t => t.MEMBER_ID)
                .Index(t => t.DEMOGRAPHY_ID);
            
            CreateTable(
                "dbo.tbl_MEMBER_DEMOGRAPHY",
                c => new
                    {
                        DEMOGRAPHY_ID = c.Long(nullable: false, identity: true),
                        DEMOGRAPHY_NAME = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.DEMOGRAPHY_ID)
                .Index(t => t.DEMOGRAPHY_NAME, unique: true);
            
            CreateTable(
                "dbo.tbl_EMPLOYEE",
                c => new
                    {
                        EMP_ID = c.Long(nullable: false, identity: true),
                        LAST_NAME = c.String(nullable: false, maxLength: 48, unicode: false),
                        FIRST_NAME = c.String(nullable: false, maxLength: 48, unicode: false),
                        TITLE = c.String(nullable: false, maxLength: 50, unicode: false),
                        EMAIL = c.String(nullable: false, maxLength: 50, unicode: false),
                        MEMBER_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EMP_ID)
                .ForeignKey("dbo.tbl_MEMBER", t => t.MEMBER_ID, cascadeDelete: true)
                .Index(t => t.EMAIL, unique: true)
                .Index(t => t.MEMBER_ID);
            
            CreateTable(
                "dbo.tbl_EQUITY",
                c => new
                    {
                        EQUITY_ID = c.Long(nullable: false, identity: true),
                        SOURCE = c.String(nullable: false, maxLength: 255, unicode: false),
                        AMOUNT = c.Double(nullable: false),
                        SURVEY_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.EQUITY_ID)
                .ForeignKey("dbo.tbl_SURVEY_DATA", t => t.SURVEY_ID, cascadeDelete: true)
                .Index(t => t.SURVEY_ID);
            
            CreateTable(
                "dbo.tbl_GRANT",
                c => new
                    {
                        GRANT_ID = c.Long(nullable: false, identity: true),
                        SOURCE = c.String(nullable: false, maxLength: 255, unicode: false),
                        AMOUNT = c.Double(nullable: false),
                        SURVEY_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.GRANT_ID)
                .ForeignKey("dbo.tbl_SURVEY_DATA", t => t.SURVEY_ID, cascadeDelete: true)
                .Index(t => t.SURVEY_ID);
            
            CreateTable(
                "dbo.tbl_MEMBER_STATUS",
                c => new
                    {
                        STATUS_ID = c.Long(nullable: false, identity: true),
                        STATUS_NAME = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.STATUS_ID);
            
            CreateTable(
                "dbo.tbl_NEWS_LINK",
                c => new
                    {
                        LINK_ID = c.Long(nullable: false, identity: true),
                        LINK = c.String(nullable: false, maxLength: 255, unicode: false),
                        MEMBER_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.LINK_ID)
                .ForeignKey("dbo.tbl_MEMBER", t => t.MEMBER_ID, cascadeDelete: true)
                .Index(t => t.MEMBER_ID);
            
            CreateTable(
                "dbo.tbl_PHONE",
                c => new
                    {
                        PHONE_ID = c.Long(nullable: false, identity: true),
                        TYPE = c.String(nullable: false, maxLength: 50, unicode: false),
                        NUMBER = c.String(nullable: false, maxLength: 20, unicode: false),
                        EMP_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.PHONE_ID)
                .ForeignKey("dbo.tbl_EMPLOYEE", t => t.EMP_ID, cascadeDelete: true)
                .Index(t => t.EMP_ID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.tbl_STATUS_HISTORY",
                c => new
                    {
                        MEMBER_ID = c.Long(nullable: false),
                        STATUS_ID = c.Long(nullable: false),
                        STATUS_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => new { t.MEMBER_ID, t.STATUS_ID })
                .ForeignKey("dbo.tbl_MEMBER", t => t.MEMBER_ID, cascadeDelete: true)
                .ForeignKey("dbo.tbl_MEMBER_STATUS", t => t.STATUS_ID, cascadeDelete: true)
                .Index(t => t.MEMBER_ID)
                .Index(t => t.STATUS_ID);
            
            CreateTable(
                "dbo.tbl_SURVEY_TOKEN",
                c => new
                    {
                        MEMBER_ID = c.Long(nullable: false),
                        TOKEN = c.String(nullable: false, maxLength: 1024, unicode: false),
                        GEN_DATE = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.MEMBER_ID)
                .ForeignKey("dbo.tbl_MEMBER", t => t.MEMBER_ID)
                .Index(t => t.MEMBER_ID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.tbl_SURVEY_TOKEN", "MEMBER_ID", "dbo.tbl_MEMBER");
            DropForeignKey("dbo.tbl_STATUS_HISTORY", "STATUS_ID", "dbo.tbl_MEMBER_STATUS");
            DropForeignKey("dbo.tbl_STATUS_HISTORY", "MEMBER_ID", "dbo.tbl_MEMBER");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.tbl_PHONE", "EMP_ID", "dbo.tbl_EMPLOYEE");
            DropForeignKey("dbo.tbl_NEWS_LINK", "MEMBER_ID", "dbo.tbl_MEMBER");
            DropForeignKey("dbo.tbl_GRANT", "SURVEY_ID", "dbo.tbl_SURVEY_DATA");
            DropForeignKey("dbo.tbl_EQUITY", "SURVEY_ID", "dbo.tbl_SURVEY_DATA");
            DropForeignKey("dbo.tbl_EMPLOYEE", "MEMBER_ID", "dbo.tbl_MEMBER");
            DropForeignKey("dbo.tbl_DEMOGRAPY_INFO", "DEMOGRAPHY_ID", "dbo.tbl_MEMBER_DEMOGRAPHY");
            DropForeignKey("dbo.tbl_DEMOGRAPY_INFO", "MEMBER_ID", "dbo.tbl_MEMBER");
            DropForeignKey("dbo.tbl_DEBT", "SURVEY_ID", "dbo.tbl_SURVEY_DATA");
            DropForeignKey("dbo.tbl_SURVEY_DATA", "MEMBER_ID", "dbo.tbl_MEMBER");
            DropForeignKey("dbo.tbl_ATTENDEE", "EVENT_ID", "dbo.tbl_EVENT");
            DropForeignKey("dbo.tbl_ATTACHMENT", "MEMBER_ID", "dbo.tbl_MEMBER");
            DropForeignKey("dbo.tbl_MEMBER", "REFERRER_ID", "dbo.tbl_REFERRER");
            DropForeignKey("dbo.tbl_MEMBER", "LEGAL_ENTITY_ID", "dbo.tbl_LEGAL_ENTITY_TYPE");
            DropForeignKey("dbo.tbl_MEMBER", "TYPE_ID", "dbo.tbl_INTERNAL_MEMBER_TYPE");
            DropForeignKey("dbo.tbl_MEMBER", "ADDRESS_ID", "dbo.tbl_ADDRESS");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.tbl_SURVEY_TOKEN", new[] { "MEMBER_ID" });
            DropIndex("dbo.tbl_STATUS_HISTORY", new[] { "STATUS_ID" });
            DropIndex("dbo.tbl_STATUS_HISTORY", new[] { "MEMBER_ID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.tbl_PHONE", new[] { "EMP_ID" });
            DropIndex("dbo.tbl_NEWS_LINK", new[] { "MEMBER_ID" });
            DropIndex("dbo.tbl_GRANT", new[] { "SURVEY_ID" });
            DropIndex("dbo.tbl_EQUITY", new[] { "SURVEY_ID" });
            DropIndex("dbo.tbl_EMPLOYEE", new[] { "MEMBER_ID" });
            DropIndex("dbo.tbl_EMPLOYEE", new[] { "EMAIL" });
            DropIndex("dbo.tbl_MEMBER_DEMOGRAPHY", new[] { "DEMOGRAPHY_NAME" });
            DropIndex("dbo.tbl_DEMOGRAPY_INFO", new[] { "DEMOGRAPHY_ID" });
            DropIndex("dbo.tbl_DEMOGRAPY_INFO", new[] { "MEMBER_ID" });
            DropIndex("dbo.tbl_SURVEY_DATA", new[] { "MEMBER_ID" });
            DropIndex("dbo.tbl_DEBT", new[] { "SURVEY_ID" });
            DropIndex("dbo.tbl_ATTENDEE", new[] { "EVENT_ID" });
            DropIndex("dbo.tbl_REFERRER", new[] { "SOURCE_NAME" });
            DropIndex("dbo.tbl_LEGAL_ENTITY_TYPE", new[] { "NAME" });
            DropIndex("dbo.tbl_INTERNAL_MEMBER_TYPE", new[] { "TYPE_NAME" });
            DropIndex("dbo.tbl_MEMBER", new[] { "REFERRER_ID" });
            DropIndex("dbo.tbl_MEMBER", new[] { "ADDRESS_ID" });
            DropIndex("dbo.tbl_MEMBER", new[] { "TYPE_ID" });
            DropIndex("dbo.tbl_MEMBER", new[] { "LEGAL_ENTITY_ID" });
            DropIndex("dbo.tbl_MEMBER", "Idx_member_name");
            DropIndex("dbo.tbl_ATTACHMENT", new[] { "MEMBER_ID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.tbl_SURVEY_TOKEN");
            DropTable("dbo.tbl_STATUS_HISTORY");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.tbl_PHONE");
            DropTable("dbo.tbl_NEWS_LINK");
            DropTable("dbo.tbl_MEMBER_STATUS");
            DropTable("dbo.tbl_GRANT");
            DropTable("dbo.tbl_EQUITY");
            DropTable("dbo.tbl_EMPLOYEE");
            DropTable("dbo.tbl_MEMBER_DEMOGRAPHY");
            DropTable("dbo.tbl_DEMOGRAPY_INFO");
            DropTable("dbo.tbl_SURVEY_DATA");
            DropTable("dbo.tbl_DEBT");
            DropTable("dbo.tbl_EVENT");
            DropTable("dbo.tbl_ATTENDEE");
            DropTable("dbo.tbl_REFERRER");
            DropTable("dbo.tbl_LEGAL_ENTITY_TYPE");
            DropTable("dbo.tbl_INTERNAL_MEMBER_TYPE");
            DropTable("dbo.tbl_MEMBER");
            DropTable("dbo.tbl_ATTACHMENT");
            DropTable("dbo.tbl_ADDRESS");
        }
    }
}

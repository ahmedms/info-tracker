namespace CompanyInformationTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewAddressLine2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tbl_ADDRESS", "ADDITIONAL", c => c.String(maxLength: 35, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tbl_ADDRESS", "ADDITIONAL", c => c.String(nullable: false, maxLength: 35, unicode: false));
        }
    }
}

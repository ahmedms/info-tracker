﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompanyInformationTracker.Models;

namespace CompanyInformationTracker.Controllers
{
    public class EventController : Controller
    {
        // GET: Event
        public ActionResult Index()
        {
            return PartialView("List");
        }

        // POST: Member/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            string eventTitle = collection.GetValue("EventTitle").AttemptedValue;
            string eventType = collection.GetValue("EventType").AttemptedValue;
            DateTime eventDate = DateTime.Parse(collection.GetValue("EventDate").AttemptedValue);
            string location = collection.GetValue("Location").AttemptedValue; 
            long eventDuration = long.Parse(collection.GetValue("EventDuration").AttemptedValue);
            string description = collection.GetValue("Description").AttemptedValue;

            ApplicationDbContext db = new ApplicationDbContext();

            Event iLabEvent = new Event
            {
                EventTitle = eventTitle,
                EventType = eventType,
                EventDate = eventDate,
                Location = location,
                EventDuration = eventDuration,
                Description = description
            };

            db.Events.Add(iLabEvent);
            db.SaveChanges();
            db.Dispose();

            return Index();

        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult GetEvents()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var events = db.Events.OrderBy(a => a.Id).ToList();
                var json = Json(new { data = events }, JsonRequestBehavior.AllowGet);
                return json;
            }
        }

    }
}
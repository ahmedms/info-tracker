﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompanyInformationTracker.Models;

namespace CompanyInformationTracker.Controllers
{
    [Authorize]
    public class EventsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Events
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetAllEvents()
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            return Json(new { data = db.Events.Where(x => x.EventDate > date).OrderBy(x => x.EventDate).ToList() }, JsonRequestBehavior.AllowGet);
        }

        // GET: Events/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // GET: Events/Create
        [HttpGet]
        public ActionResult Create()
        {
            return PartialView("Create");
        }

        // POST: Events/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Event @event)
        {
            bool status = false;
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            if (ModelState.IsValid)
            {
                db.Events.Add(@event);
                db.SaveChanges();

                status = true;
            }

            return new JsonResult { Data = new { status = status } };
        }

        // GET: Events/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return PartialView(@event);
        }

        // POST: Events/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Event @event)
        {
            bool status = false;

            if (ModelState.IsValid)
            {
                db.Entry(@event).State = EntityState.Modified;
                db.SaveChanges();

                status = true;
                
            }

            return new JsonResult { Data = new { status = status } };

        }

        // GET: Events/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Events.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return PartialView(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Event @event = db.Events.Find(id);
            db.Events.Remove(@event);
            db.SaveChanges();

            return new JsonResult { Data = new { status = true } };
        }

        public ActionResult GetAttendees(long eventId)
        {
            return Json(new { data = db.Attendees.Where(m => m.EVENT_ID == eventId).ToList() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult NewAttendee(long? eventId)
        {
            if (eventId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.EventId = eventId.Value;
            return PartialView("Attendee/Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewAttendee(Attendee attendee)
        {
            bool status = false;
            
            if (ModelState.IsValid)
            {
                db.Attendees.Add(attendee);
                db.SaveChanges();

                status = true;
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult EditAttendee(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendee attendee = db.Attendees.Find(id);
            if (attendee == null)
            {
                return HttpNotFound();
            }
            return PartialView("Attendee/Edit", attendee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAttendee(Attendee attendee)
        {
            bool status = false;

            if (ModelState.IsValid)
            {
                db.Entry(attendee).State = EntityState.Modified;
                db.SaveChanges();

                status = true;

            }

            return new JsonResult { Data = new { status = status } };

        }

        // GET: Events/Delete/5
        public ActionResult DeleteAttendee(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Attendee attendee = db.Attendees.Find(id);
            if (attendee == null)
            {
                return HttpNotFound();
            }
            return PartialView("Attendee/Delete", attendee);
        }

        [HttpPost, ActionName("DeleteAttendee")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteAttendeeConfirmed(long id)
        {
            Attendee attendee = db.Attendees.Find(id);
            db.Attendees.Remove(attendee);
            db.SaveChanges();

            return new JsonResult { Data = new { status = true } };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

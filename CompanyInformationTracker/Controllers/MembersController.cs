﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.IO;
using System.Data.Entity.Infrastructure;
using CompanyInformationTracker.Models;

namespace CompanyInformationTracker.Controllers
{
    [Authorize]
    public class MembersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private class MemberInfo
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public string IncorpDate { get; set; }
            public string NaicsCode { get; set; }

            public MemberInfo(Member member)
            {
                Id = member.Id;
                Name = member.MemberName;
                IncorpDate = member.DateIncorp.ToLongDateString();
                NaicsCode = member.NaicsCode;
            }

        }

        // GET: Events
        public ActionResult Index()
        {
            return PartialView("List");
        }

        public ActionResult GetMembers()
        {
            var members = db.Members.Where(m=>m.IsActive == true).OrderByDescending(a => a.DateIncorp).ToList();

            List<MemberInfo> memberList = new List<MemberInfo>();

            foreach (Member member in members)
            {
                memberList.Add(new MemberInfo(member));
            }

            return Json(new { data = memberList }, JsonRequestBehavior.AllowGet);
        }

        // GET: Members/Create
        [HttpGet]
        public ActionResult Create()
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            Member member = new Member
            {
                InternalTypeList = db.InternalMemberTypes.OrderBy(m => m.Id).ToList(),
                LegalEntityTypeList = db.LegalEntityTypes.OrderBy(m => m.Id).ToList(),
                ReferrerList = db.Referrers.OrderBy(m => m.Id).ToList(),
                DateIncorp = date,
                DateEntered = date
            };

            DemographyInfo demography = new DemographyInfo
            {
                MemberDemographyList = db.MemberDemographies.OrderBy(m => m.Id).ToList()
            };

            StatusHistory statusHistory = new StatusHistory
            {
                StatusDate = date,
                MemberStatusList = db.MemberStatuses.OrderBy(m => m.Id).ToList()
            };

            MemberViewModel mvm = new MemberViewModel
            {
                Member = member,
                DemographyInfo = demography,
                StatusHistory = statusHistory,
            };

            return PartialView("Create", mvm);
        }

        // POST: Members/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MemberViewModel model)
        {
            bool status = false;
            //var errors = ModelState.Values.SelectMany(v => v.Errors);

            if (ModelState.IsValid)
            {
                string additionalLine = "";
                string website = "";
                string description = "";

                if (!String.IsNullOrEmpty(model.Member.AdditionalLine)) additionalLine = model.Member.AdditionalLine;
                if (!String.IsNullOrEmpty(model.Member.Website)) website = model.Member.Website;
                if (!String.IsNullOrEmpty(model.Member.Description)) description = model.Member.Description;

                SqlParameter[] parameters = new SqlParameter[]
                {
                    //comapny information
                    new SqlParameter() {ParameterName = "member_name", SqlDbType = SqlDbType.VarChar, Value= model.Member.MemberName},
                    new SqlParameter() {ParameterName = "date_incorp", SqlDbType = SqlDbType.DateTime2, Value= model.Member.DateIncorp},
                    new SqlParameter() {ParameterName = "date_entered", SqlDbType = SqlDbType.DateTime2, Value= model.Member.DateEntered},
                    new SqlParameter() {ParameterName = "naicscode", SqlDbType = SqlDbType.VarChar, Value= model.Member.NaicsCode},
                    new SqlParameter() {ParameterName = "website", SqlDbType = SqlDbType.VarChar, Value= website},
                    new SqlParameter() {ParameterName = "description", SqlDbType = SqlDbType.VarChar, Value= description},
                    new SqlParameter() {ParameterName = "legal_entity_id", SqlDbType = SqlDbType.BigInt, Value= model.Member.LEGAL_ENTITY_ID},
                    new SqlParameter() {ParameterName = "member_type_id", SqlDbType = SqlDbType.BigInt, Value= model.Member.TYPE_ID},
                    new SqlParameter() {ParameterName = "referre_id", SqlDbType = SqlDbType.BigInt, Value= model.Member.REFERRER_ID},
                    
                    //Address information
                    new SqlParameter() {ParameterName = "street_no", SqlDbType = SqlDbType.VarChar, Value= model.Member.StreetNo},
                    new SqlParameter() {ParameterName = "additional", SqlDbType = SqlDbType.VarChar, Value= additionalLine},
                    new SqlParameter() {ParameterName = "city", SqlDbType = SqlDbType.VarChar, Value= model.Member.City},
                    new SqlParameter() {ParameterName = "state", SqlDbType = SqlDbType.VarChar, Value= model.Member.State},
                    new SqlParameter() {ParameterName = "postalcode", SqlDbType = SqlDbType.VarChar, Value= model.Member.PostalCode},
                    new SqlParameter() {ParameterName = "country", SqlDbType = SqlDbType.VarChar, Value= model.Member.Country},
                    new SqlParameter() {ParameterName = "lastupdate", SqlDbType = SqlDbType.DateTime2, Value= DateTime.Now},

                    //Company indentification information
                    new SqlParameter() {ParameterName = "demography_id", SqlDbType = SqlDbType.BigInt, Value= model.DemographyInfo.DEMOGRAPHY_ID},
                    new SqlParameter() {ParameterName = "status_id", SqlDbType = SqlDbType.BigInt, Value= model.StatusHistory.STATUS_ID},
                    new SqlParameter() {ParameterName = "status_date", SqlDbType = SqlDbType.DateTime2, Value= model.StatusHistory.StatusDate},

                    //Company contact information
                    new SqlParameter() {ParameterName = "first_name", SqlDbType = SqlDbType.VarChar, Value= model.Employee.FirstName},
                    new SqlParameter() {ParameterName = "last_name", SqlDbType = SqlDbType.VarChar, Value= model.Employee.LastName},
                    new SqlParameter() {ParameterName = "title", SqlDbType = SqlDbType.VarChar, Value= model.Employee.Title},
                    new SqlParameter() {ParameterName = "email", SqlDbType = SqlDbType.VarChar, Value= model.Employee.Email},

                    //Contact phone
                    new SqlParameter() {ParameterName = "phone_type", SqlDbType = SqlDbType.VarChar, Value= model.Employee.PhoneType},
                    new SqlParameter() {ParameterName = "phone_number", SqlDbType = SqlDbType.VarChar, Value= model.Employee.PhoneNumber},
                };

                db.Database.ExecuteSqlCommand("sp_member_insert @member_name, @date_incorp, @date_entered, @naicscode, @website, @description, @legal_entity_id, @member_type_id, @referre_id, @street_no, @additional, @city, @state, @postalcode, @country, @lastupdate, @demography_id, @status_id, @status_date, @first_name, @last_name, @title, @email, @phone_type, @phone_number", parameters);

                status = true;
            }

            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            string result = "Member information could not be saved at " + date.ToShortTimeString() + " on " + DateTime.Now.ToLongDateString();
            if (status)
                result = "Member information saved successfully at " + date.ToShortTimeString() + " on " + DateTime.Now.ToLongDateString();

            return new JsonResult { Data = result };
        }

        // GET: Members/Create
        [HttpGet]
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }

            member.InternalTypeList = db.InternalMemberTypes.OrderBy(m => m.Id).ToList();
            member.LegalEntityTypeList = db.LegalEntityTypes.OrderBy(m => m.Id).ToList();
            member.ReferrerList = db.Referrers.OrderBy(m => m.Id).ToList();

            DemographyInfo demography = db.DemographyInfo.Where(m => m.MEMBER_ID == member.Id).FirstOrDefault();
            demography.MemberDemographyList = db.MemberDemographies.OrderBy(m => m.Id).ToList();
            
            Employee employee = db.Employees.Where(m => m.MEMBER_ID == member.Id && m.IsPrimary == true).FirstOrDefault();

            StatusHistory statusHistory = db.StatusHistories.Where(m => m.MEMBER_ID == member.Id).OrderByDescending(m => m.StatusDate).FirstOrDefault();
            statusHistory.MemberStatusList = db.MemberStatuses.OrderBy(m => m.Id).ToList();
            
            MemberViewModel memberViewModel = new MemberViewModel
            {
                Member = member,
                DemographyInfo = demography,
                StatusHistory = statusHistory,
                Employee = employee
            };

            return PartialView("Edit", memberViewModel);
        }

        // POST: Members/Edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MemberViewModel model)
        {
            bool status = false;
            var errors = ModelState.Values.SelectMany(v => v.Errors);

            if (ModelState.IsValid)
            {
                string additionalLine = "";
                string website = "";
                string description = "";

                if (!String.IsNullOrEmpty(model.Member.AdditionalLine)) additionalLine = model.Member.AdditionalLine;
                if (!String.IsNullOrEmpty(model.Member.Website)) website = model.Member.Website;
                if (!String.IsNullOrEmpty(model.Member.Description)) description = model.Member.Description;

                SqlParameter[] parameters = new SqlParameter[]
                {
                    //comapny information
                    new SqlParameter() {ParameterName = "member_id", SqlDbType = SqlDbType.BigInt, Value= model.Member.Id},
                    new SqlParameter() {ParameterName = "member_name", SqlDbType = SqlDbType.VarChar, Value= model.Member.MemberName},
                    new SqlParameter() {ParameterName = "date_incorp", SqlDbType = SqlDbType.DateTime2, Value= model.Member.DateIncorp},
                    new SqlParameter() {ParameterName = "date_entered", SqlDbType = SqlDbType.DateTime2, Value= model.Member.DateEntered},
                    new SqlParameter() {ParameterName = "naicscode", SqlDbType = SqlDbType.VarChar, Value= model.Member.NaicsCode},
                    new SqlParameter() {ParameterName = "website", SqlDbType = SqlDbType.VarChar, Value= website},
                    new SqlParameter() {ParameterName = "description", SqlDbType = SqlDbType.VarChar, Value= description},
                    new SqlParameter() {ParameterName = "legal_entity_id", SqlDbType = SqlDbType.BigInt, Value= model.Member.LEGAL_ENTITY_ID},
                    new SqlParameter() {ParameterName = "member_type_id", SqlDbType = SqlDbType.BigInt, Value= model.Member.TYPE_ID},
                    new SqlParameter() {ParameterName = "referre_id", SqlDbType = SqlDbType.BigInt, Value= model.Member.REFERRER_ID},
                    
                    //Address information
                    new SqlParameter() {ParameterName = "street_no", SqlDbType = SqlDbType.VarChar, Value= model.Member.StreetNo},
                    new SqlParameter() {ParameterName = "additional", SqlDbType = SqlDbType.VarChar, Value= additionalLine },
                    new SqlParameter() {ParameterName = "city", SqlDbType = SqlDbType.VarChar, Value= model.Member.City},
                    new SqlParameter() {ParameterName = "state", SqlDbType = SqlDbType.VarChar, Value= model.Member.State},
                    new SqlParameter() {ParameterName = "postalcode", SqlDbType = SqlDbType.VarChar, Value= model.Member.PostalCode},
                    new SqlParameter() {ParameterName = "country", SqlDbType = SqlDbType.VarChar, Value= model.Member.Country},
                    new SqlParameter() {ParameterName = "lastupdate", SqlDbType = SqlDbType.DateTime2, Value= DateTime.Now},

                    //Company indentification information
                    new SqlParameter() {ParameterName = "demography_id", SqlDbType = SqlDbType.BigInt, Value= model.DemographyInfo.DEMOGRAPHY_ID},
                    new SqlParameter() {ParameterName = "status_id", SqlDbType = SqlDbType.BigInt, Value= model.StatusHistory.STATUS_ID},
                    new SqlParameter() {ParameterName = "status_date", SqlDbType = SqlDbType.DateTime2, Value= model.StatusHistory.StatusDate},

                    //Company contact information
                    new SqlParameter() {ParameterName = "employee_id", SqlDbType = SqlDbType.BigInt, Value= model.Employee.Id},
                    new SqlParameter() {ParameterName = "first_name", SqlDbType = SqlDbType.VarChar, Value= model.Employee.FirstName},
                    new SqlParameter() {ParameterName = "last_name", SqlDbType = SqlDbType.VarChar, Value= model.Employee.LastName},
                    new SqlParameter() {ParameterName = "title", SqlDbType = SqlDbType.VarChar, Value= model.Employee.Title},
                    new SqlParameter() {ParameterName = "email", SqlDbType = SqlDbType.VarChar, Value= model.Employee.Email},

                    //Contact phone
                    new SqlParameter() {ParameterName = "phone_type", SqlDbType = SqlDbType.VarChar, Value= model.Employee.PhoneType},
                    new SqlParameter() {ParameterName = "phone_number", SqlDbType = SqlDbType.VarChar, Value= model.Employee.PhoneNumber},
                };

                db.Database.ExecuteSqlCommand("sp_member_update @member_id, @member_name, @date_incorp, @date_entered, @naicscode, @website, @description, @legal_entity_id, @member_type_id, @referre_id, @street_no, @additional, @city, @state, @postalcode, @country, @lastupdate, @demography_id, @status_id, @status_date, @employee_id, @first_name, @last_name, @title, @email, @phone_type, @phone_number", parameters);

                status = true;
            }
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            string result = "Member information could not be saved at " + date.ToShortTimeString() + " on " + DateTime.Now.ToLongDateString();
            if(status)
                result = "Member information saved successfully at " + date.ToShortTimeString() + " on " + DateTime.Now.ToLongDateString();

            return new JsonResult { Data = result };
        }

        
        
        // GET: Members/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Member member = db.Members.Find(id);
            if (member == null)
            {
                return HttpNotFound();
            }
            return PartialView(member);
        }

        // POST: Members/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            Member member = db.Members.Find(id);
            db.Members.Attach(member);
            member.IsActive = false;
            member.LastUpdate = date;
            var entry = db.Entry(member);
            entry.Property(e => e.IsActive).IsModified = true;
            entry.Property(e => e.LastUpdate).IsModified = true;

            db.SaveChanges();

            return new JsonResult { Data = new { status = true } };
        }


        // GET: Members/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string sqlQuery = "SELECT TOP 1 * FROM MEMBER_INFO WHERE MEMBER_ID = @p0 ORDER BY STATUS_DATE DESC";

            //IEnumerable<MemberView> results = db.Database.SqlQuery(sqlQuery, id.Value);

            MemberView model = db.Database.SqlQuery<MemberView>(sqlQuery, id.Value).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Details", model);
        }

        private class EmployeeInfo
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public bool IsPrimary { get; set; }
        }
        
        public ActionResult GetEmployees(long memberId)
        {
            var results = db.Employees.Where(m => m.MEMBER_ID == memberId).ToList();
            var list = new List<EmployeeInfo>();
            foreach(var employee in results)
            {
                list.Add(new EmployeeInfo
                {
                    Id = employee.Id,
                    Name = employee.Title + " " + employee.FirstName + " " + employee.LastName,
                    Phone =  employee.PhoneNumber + "(" + employee.PhoneType + ")",
                    Email = employee.Email,
                    IsPrimary = employee.IsPrimary
                });
            }
            return Json(new { data = list.ToList() }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public ActionResult NewEmployee(long? memberId)
        {
            if (memberId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.MemberId = memberId.Value;
            return PartialView("Employee/Create");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewEmployee(Employee employee)
        {
            bool status = false;

            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);
                db.SaveChanges();

                status = true;
            }

            return new JsonResult { Data = new { status = status } };
        }
        
        public ActionResult EditEmployee(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return PartialView("Employee/Edit", employee);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee(Employee employee)
        {
            bool status = false;

            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();

                status = true;

            }

            return new JsonResult { Data = new { status = status } };

        }


        public ActionResult DeleteEmployee(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return PartialView("Employee/Delete", employee);
        }

        [HttpPost, ActionName("DeleteEmployee")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteEmployeeConfirmed(long id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();

            return new JsonResult { Data = new { status = true } };
        }

        private class StatusDBModel
        {
            public string STATUS_NAME { get; set; }
            public DateTime STATUS_DATE { get; set; }
        }

        private class StatusInfo
        {
            public string STATUS_NAME { get; set; }
            public string STATUS_DATE { get; set; }
        }

        public ActionResult GetStatusHistory(long memberId)
        {
            string sqlQuery = "SELECT s.STATUS_NAME, sh.STATUS_DATE " +
                                "FROM tbl_STATUS_HISTORY sh " +
                                "JOIN tbl_MEMBER_STATUS s " +
                                "ON sh.STATUS_ID = s.STATUS_ID " +
                                "WHERE sh.MEMBER_ID = @p0 ORDER BY sh.STATUS_DATE DESC";


            var results = db.Database.SqlQuery<StatusDBModel>(sqlQuery, memberId).ToList();

            var list = new List<StatusInfo>();

            foreach(var result in results)
            {
                list.Add(new StatusInfo
                {
                    STATUS_NAME = result.STATUS_NAME,
                    STATUS_DATE = result.STATUS_DATE.ToLongDateString()
                });
            }

            return Json(new { data = list.ToList() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Upload(long memberId)
        {
            ViewBag.MemberId = memberId;
            return PartialView("Attachment/Upload");
        }

        public ActionResult GetAttachments(long memberId)
        {
            return Json(new { data = db.Attachments.Where(m => m.MEMBER_ID == memberId).ToList() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadFile(long memberId)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var memberName = db.Members.Where(x => x.Id == memberId).Select(x => x.MemberName).FirstOrDefault().ToString().Replace(" ", "-");
                    var myPath = memberName + "-" + fileName;

                    TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

                    var attachment = new Attachment
                    {
                        AttachmentName = fileName,
                        Path = myPath,
                        Date = date,
                        MEMBER_ID = memberId
                    };

                    var path = Path.Combine(Server.MapPath("~/App_Data/"), myPath);
                    file.SaveAs(path);

                    db.Attachments.Add(attachment);
                    db.SaveChanges();

                    return RedirectToAction("Details", new { id = memberId });
                }
            }
            return RedirectToAction("Upload", new { memberId = memberId });
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

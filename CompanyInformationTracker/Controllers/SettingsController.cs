﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompanyInformationTracker.Models;

namespace CompanyInformationTracker.Controllers
{
    [Authorize]
    public class SettingsController : Controller
    {
        //The following actions for CRUDing internal member types
        public PartialViewResult MemberType()
        {
            return PartialView("InternalType/List");
        }

        public ActionResult GetInternalTypes()
        {
            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                var types = db.InternalMemberTypes.OrderBy(a => a.Id).ToList();
                return Json(new { data = types }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult CreateInternalType()
        {
            return PartialView("InternalType/Create");
        }

        [HttpPost]
        public ActionResult CreateInternalType(InternalMemberType type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.InternalMemberTypes.Add(type);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditInternalType(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.InternalMemberTypes.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("InternalType/Edit", type);
            }
        }

        [HttpPost]
        public ActionResult EditInternalType(InternalMemberType type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.InternalMemberTypes.Attach(type);
                    var entry = db.Entry(type);
                    entry.Property(e => e.TypeName).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteInternalType(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.InternalMemberTypes.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("InternalType/Delete", type);
            }
        }

        [HttpPost, ActionName("DeleteInternalType")]
        public ActionResult _DeleteInternalType(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.InternalMemberTypes.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.InternalMemberTypes.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }

        //The following actions for CRUDing legal entity types
        public PartialViewResult LegalType()
        {
            return PartialView("LegalType/List");
        }

        public ActionResult GetLegalTypes()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var types = db.LegalEntityTypes.OrderBy(a => a.Id).ToList();
                return Json(new { data = types }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult CreateLegalType()
        {
            return PartialView("LegalType/Create");
        }

        [HttpPost]
        public ActionResult CreateLegalType(LegalEntityType type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.LegalEntityTypes.Add(type);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditLegalType(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.LegalEntityTypes.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("LegalType/Edit", type);
            }
        }

        [HttpPost]
        public ActionResult EditLegalType(LegalEntityType type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.LegalEntityTypes.Attach(type);
                    var entry = db.Entry(type);
                    entry.Property(e => e.TypeName).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteLegalType(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.LegalEntityTypes.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("LegalType/Delete", type);
            }
        }

        [HttpPost, ActionName("DeleteLegalType")]
        public ActionResult _DeleteLegalType(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.LegalEntityTypes.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.LegalEntityTypes.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }

        //The following actions for CRUDing member demography information
        public PartialViewResult Demography()
        {
            return PartialView("Demography/List");
        }

        public ActionResult GetDemographies()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var types = db.MemberDemographies.OrderBy(a => a.Id).ToList();
                return Json(new { data = types }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult CreateDemography()
        {
            return PartialView("Demography/Create");
        }

        [HttpPost]
        public ActionResult CreateDemography(MemberDemography type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.MemberDemographies.Add(type);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditDemography(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.MemberDemographies.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Demography/Edit", type);
            }
        }

        [HttpPost]
        public ActionResult EditDemography(MemberDemography type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.MemberDemographies.Attach(type);
                    var entry = db.Entry(type);
                    entry.Property(e => e.DemographyName).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteDemography(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.MemberDemographies.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Demography/Delete", type);
            }
        }

        [HttpPost, ActionName("DeleteDemography")]
        public ActionResult _DeleteDemography(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.MemberDemographies.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.MemberDemographies.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }

        //The following actions for CRUDing referrer
        public PartialViewResult Referrer()
        {
            return PartialView("Referrer/List");
        }

        public ActionResult GetReferrers()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var types = db.Referrers.OrderBy(a => a.Id).ToList();
                return Json(new { data = types }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult CreateReferrer()
        {
            return PartialView("Referrer/Create");
        }

        [HttpPost]
        public ActionResult CreateReferrer(Referrer type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Referrers.Add(type);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditReferrer(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.Referrers.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Referrer/Edit", type);
            }
        }

        [HttpPost]
        public ActionResult EditReferrer(Referrer type)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Referrers.Attach(type);
                    var entry = db.Entry(type);
                    entry.Property(e => e.SourceName).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteReferrer(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.Referrers.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Referrer/Delete", type);
            }
        }

        [HttpPost, ActionName("DeleteReferrer")]
        public ActionResult _DeleteReferrer(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.Referrers.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.Referrers.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }


        //The following actions for CRUDing referrer
        public PartialViewResult Status()
        {
            return PartialView("Status/List");
        }

        public ActionResult GetStatuses()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var types = db.MemberStatuses.OrderBy(a => a.Id).ToList();
                return Json(new { data = types }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult CreateStatus()
        {
            return PartialView("Status/Create");
        }

        [HttpPost]
        public ActionResult CreateStatus(MemberStatus mstatus)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.MemberStatuses.Add(mstatus);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditStatus(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var status = db.MemberStatuses.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Status/Edit", status);
            }
        }

        [HttpPost]
        public ActionResult EditStatus(MemberStatus mstatus)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.MemberStatuses.Attach(mstatus);
                    var entry = db.Entry(mstatus);
                    entry.Property(e => e.StatusName).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteStatus(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var type = db.MemberStatuses.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Status/Delete", type);
            }
        }

        [HttpPost, ActionName("DeleteStatus")]
        public ActionResult _DeleteStatus(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.MemberStatuses.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.MemberStatuses.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompanyInformationTracker.Models;
using CompanyInformationTracker.Services;

namespace CompanyInformationTracker.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Members()
        {
            SurveyDataFetcher fetcher = new SurveyDataFetcher();
            var result = fetcher.GetMembersAggregatedSurveyData();

            return PartialView("Members", result);
        }

        public ActionResult Graduates()
        {
            SurveyDataFetcher fetcher = new SurveyDataFetcher();
            var result = fetcher.GetGraudatesAggregatedSurveyData();

            return PartialView("Graduates", result);
        }

        public ActionResult Clients()
        {
            SurveyDataFetcher fetcher = new SurveyDataFetcher();
            var result = fetcher.GetClientsAggregatedSurveyData();

            return PartialView("Clients", result);
        }

        public ActionResult Individual()
        {
            return View("Individual");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult Autocomplete(string term)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var list = db.Members
                    .Where(s => s.IsActive == true && s.MemberName.ToLower()
                    .Contains(term.ToLower()))
                    .Select(x => new { x.Id, x.MemberName })
                    .ToList();

                var result = new List<KeyValuePair<string, string>>();

                foreach (var item in list)
                {
                    result.Add(new KeyValuePair<string, string>(item.Id.ToString(), item.MemberName));
                }
                var result3 = result.Where(s => s.Value.ToLower().Contains
                              (term.ToLower())).Select(w => w).ToList();
                return Json(result3, JsonRequestBehavior.AllowGet);
            }         
        }

        public ActionResult Load(long id)
        {
            SurveyDataFetcher fetcher = new SurveyDataFetcher();
            var result = fetcher.GetAMemberAggregatedSurveyData(id);

            using(ApplicationDbContext db = new ApplicationDbContext())
            {
                ViewBag.MemberName = db.Members.Where(m => m.Id == id).Select(x => x.MemberName).FirstOrDefault().ToString();
                return PartialView("Member", result);
            }
        }
    }
}
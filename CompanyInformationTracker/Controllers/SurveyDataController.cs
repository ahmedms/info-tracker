﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using CompanyInformationTracker.Models;
using CompanyInformationTracker.Services;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CompanyInformationTracker.Controllers
{
    
    public class SurveyDataController : Controller
    {
        // GET: SurveyData
        public ActionResult Index()
        {
            /*using(IdentityDbContext db = new IdentityDbContext())
            {
                db.Roles.Add(new IdentityRole
                {
                    Name = "User"
                });
                db.SaveChanges();
            }*/

            return RedirectToAction("Individual", "Report");
        }


        [Authorize]
        public ActionResult SurveyList (long? memberId)
        {
            if (memberId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.MemberId = memberId.Value;

            return View("List");
        }

        private class SurveyInfo
        {
            public long Id { get; set; }
            public string SurveyDate { get; set; }
        }

        [Authorize]
        public ActionResult GetSurveyData(long? memberId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var list = new List<SurveyInfo>();
                var results = new List<SurveyData>();
                if(memberId != null)
                {
                    results = db.SurveyData.Where(m => m.MEMBER_ID == memberId.Value).ToList();
                }

                foreach(var r in results)
                {
                    list.Add(new SurveyInfo
                    {
                        Id = r.Id,
                        SurveyDate = r.SurveyDate.ToLongDateString()
                    });
                }

                return Json(new { data =  list}, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ActionResult SendInvitation(long? memberId, long? surveyId)
        {
            if (memberId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

                SurveyData surveyData = new SurveyData
                {
                    FullTimeEmployees = 0,
                    PartTimeEmployees = 0,
                    GrossMonthlySalary = 0,
                    GrossRevenue = 0,
                    IsBaseData = db.SurveyData
                                   .Where(m => m.MEMBER_ID == memberId.Value).ToList()
                                   .Count() > 0 ? false : true,
                    SurveyDate = date,
                    MEMBER_ID = memberId.Value

                };

                if (surveyId == null)
                {
                
                        db.SurveyData.Add(surveyData);
                        db.SaveChanges();
                
                
                }

                long id = surveyId == null ? surveyData.Id : surveyId.Value;

                string encryptedToken = AESThenHMAC.SimpleEncryptWithPassword(PrepareToken(id), "csci5250csci5250");

                UrlHelper u = new UrlHelper(this.ControllerContext.RequestContext);
                string url = u.Action("Edit", "SurveyData", new { token = encryptedToken }, this.Request.Url.Scheme);

                var employees = db.Employees.Where(m => m.MEMBER_ID == memberId.Value && m.IsPrimary == true).ToList();

                bool status = false;

                foreach(var e in employees)
                {
                    if (!String.IsNullOrEmpty(e.Email))
                    {
                        MailerAgent.send(e.Email,
                                 "Survey Data Collaboration Invitation from Innovation Lab",
                                @"Hello " + e.Title + " " + e.LastName + ", Hope you are doing well! <br> Click on the following link to enter survey data<br> <a href=" + url + ">" + url + "</a>",
                                 true);

                        status = true;
                    }
                }

                string result = "No primary contact email address found to send the link at " + date.ToShortTimeString() + " on " + date.ToShortDateString();

                if(status)
                    result = "Survey link sent at " + date.ToShortTimeString()+ " on " + date.ToShortDateString() + "!";
              
                if(surveyId == null)
                    return new JsonResult { Data = result };
                else
                {
                    ViewBag.ConfirmMessage = result;
                    return PartialView("confirmation");
                }
            }
        }

        [Authorize]
        public string PrepareToken(long surveyId)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            byte[] time = BitConverter.GetBytes(date.ToBinary());
            byte[] id = BitConverter.GetBytes(surveyId);
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(id).Concat(key).ToArray());

            return token;
        }

        [Authorize]
        public String GetBaseUrl()
        {
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
            return baseUrl;
        }

        [AllowAnonymous]
        public ActionResult Edit(string token)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            string decryptedToken = AESThenHMAC.SimpleDecryptWithPassword(token, "csci5250csci5250");
            byte[] data = Convert.FromBase64String(decryptedToken);
            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
            long surveyId = BitConverter.ToInt64(data, 8);

            if(when  < date.AddHours(-24))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var surveyData = db.SurveyData.Where(m => m.Id == surveyId).FirstOrDefault();
                return View("Edit", surveyData);
            }
        }

        [Authorize]
        public ActionResult Details(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var surveyData = db.SurveyData.Where(m => m.Id == id).FirstOrDefault();
                
                return View("Edit", surveyData);
            }
        }

        [HttpPost]
        public ActionResult Edit(SurveyData survey)
        {
            TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);

            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.SurveyData.Attach(survey);
                    survey.UpdateDate = date;

                    var entry = db.Entry(survey);
                    entry.Property(e => e.SurveyDate).IsModified = true;
                    entry.Property(e => e.FullTimeEmployees).IsModified = true;
                    entry.Property(e => e.PartTimeEmployees).IsModified = true;
                    entry.Property(e => e.GrossMonthlySalary).IsModified = true;
                    entry.Property(e => e.GrossRevenue).IsModified = true;
                    entry.Property(e => e.UngoMerge).IsModified = true;
                    entry.Property(e => e.UpdateDate).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }
            
            string result = "";
            if (status)
            {
                result = "Survey data has been successfully updated at " + date.ToShortTimeString() + " on " + date.ToShortDateString();
            }
            else
            {
                result = "Survey data update failed at " + date.ToShortTimeString() + " on " + date.ToShortDateString();
            }

            return new JsonResult { Data = result };
        }


        //The following actions for CRUDing grants
        public PartialViewResult Grant(long surveyId)
        {
            ViewBag.MySurveyId = surveyId;
            return PartialView("Grant/List");
        }

        public ActionResult GetGrants(long surveyId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var grants = db.Grants.Where(m => m.SURVEY_ID == surveyId).OrderBy(m => m.Id).ToList();
                var list = Enumerable.Empty<object>()
                                     .Select(r => new { Id = (long)0, Source = "", Amount = (double)0 })
                                     .ToList();

                foreach (Grant grant in grants)
                {
                    list.Add(new
                    {
                        Id = grant.Id,
                        Source = grant.Source,
                        Amount = grant.Amount
                    });
                }

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult NewGrant(long surveyId)
        {
            ViewBag.MySurveyId = surveyId;
            return PartialView("Grant/Create");
        }

        [HttpPost]
        public ActionResult NewGrant(Grant grant)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Grants.Add(grant);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditGrant(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var grant = db.Grants.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Grant/Edit", grant);
            }
        }

        [HttpPost]
        public ActionResult EditGrant(Grant grant)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Grants.Attach(grant);
                    var entry = db.Entry(grant);
                    entry.Property(e => e.Source).IsModified = true;
                    entry.Property(e => e.Amount).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteGrant(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var grant = db.Grants.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Grant/Delete", grant);
            }
        }

        [HttpPost, ActionName("DeleteGrant")]
        public ActionResult _DeleteGrant(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.Grants.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.Grants.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }



        //The following actions for CRUDing debts
        public PartialViewResult Debt(long surveyId)
        {
            ViewBag.MySurveyId = surveyId;
            return PartialView("Debt/List");
        }

        public ActionResult GetDebts(long surveyId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var debts = db.Debts.Where(m => m.SURVEY_ID == surveyId).OrderBy(m => m.Id).ToList();
                var list = Enumerable.Empty<object>()
                                     .Select(r => new { Id = (long)0, Source = "", Amount = (double)0 })
                                     .ToList();

                foreach (Debt debt in debts)
                {
                    list.Add(new
                    {
                        Id = debt.Id,
                        Source = debt.Source,
                        Amount = debt.Amount
                    });
                }

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult NewDebt(long surveyId)
        {
            ViewBag.MySurveyId = surveyId;
            return PartialView("Debt/Create");
        }

        [HttpPost]
        public ActionResult NewDebt(Debt debt)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Debts.Add(debt);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditDebt(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var debt = db.Debts.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Debt/Edit", debt);
            }
        }

        [HttpPost]
        public ActionResult EditDebt(Debt debt)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Debts.Attach(debt);
                    var entry = db.Entry(debt);
                    entry.Property(e => e.Source).IsModified = true;
                    entry.Property(e => e.Amount).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteDebt(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var debt = db.Debts.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Debt/Delete", debt);
            }
        }

        [HttpPost, ActionName("DeleteDebt")]
        public ActionResult _DeleteDebt(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.Debts.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.Debts.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }




        //The following actions for CRUDing debts
        public PartialViewResult Equity(long surveyId)
        {
            ViewBag.MySurveyId = surveyId;
            return PartialView("Equity/List");
        }

        public ActionResult GetEquities(long surveyId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var equities = db.Equities.Where(m => m.SURVEY_ID == surveyId).OrderBy(m => m.Id).ToList();
                var list = Enumerable.Empty<object>()
                                     .Select(r => new { Id = (long)0, Source = "", Amount = (double)0 })
                                     .ToList();

                foreach (Equity equity in equities)
                {
                    list.Add(new
                    {
                        Id = equity.Id,
                        Source = equity.Source,
                        Amount = equity.Amount
                    });
                }

                return Json(new { data = list }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult NewEquity(long surveyId)
        {
            ViewBag.MySurveyId = surveyId;
            return PartialView("Equity/Create");
        }

        [HttpPost]
        public ActionResult NewEquity(Equity equity)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Equities.Add(equity);
                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult EditEquity(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var equity = db.Equities.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Equity/Edit", equity);
            }
        }

        [HttpPost]
        public ActionResult EditEquity(Equity equity)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    db.Equities.Attach(equity);
                    var entry = db.Entry(equity);
                    entry.Property(e => e.Source).IsModified = true;
                    entry.Property(e => e.Amount).IsModified = true;

                    db.SaveChanges();
                    status = true;
                }
            }

            return new JsonResult { Data = new { status = status } };
        }

        public ActionResult DeleteEquity(long id)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var equity = db.Equities.Where(row => row.Id == id).FirstOrDefault();
                return PartialView("Equity/Delete", equity);
            }
        }

        [HttpPost, ActionName("DeleteEquity")]
        public ActionResult _DeleteEquity(long id)
        {
            bool status = false;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var v = db.Equities.Where(a => a.Id == id).FirstOrDefault();
                if (v != null)
                {
                    db.Equities.Remove(v);
                    db.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}
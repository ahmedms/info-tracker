﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using CompanyInformationTracker.Models;

namespace CompanyInformationTracker.Services
{
    public class SurveyDataFetcher
    {
        public List<SurveyDBModelExtended> GetMembersAggregatedSurveyData()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter {ParameterName = "member_id", SqlDbType = SqlDbType.BigInt, Value =  DBNull.Value},
                };

                var results = db.Database.SqlQuery<SurveyDBModel>("sp_report_member @member_id", parameters).ToList();

                var groupList = results.GroupBy(m => m.MemberId)
                    .Select(grp => grp.ToList())
                    .ToList();

                int maxItems = 0;
                foreach (var item in groupList)
                {
                    if (item.Count() > maxItems) maxItems = item.Count();
                }

                var newList = new List<SurveyDBModelExtended>();

                for (int i = 0; i < maxItems; i++)
                {
                    var tempList = new List<SurveyDBModel>();
                    foreach (var group in groupList)
                    {
                        if (group.Count > i)
                            tempList.Add(group[i]);
                    }
                    newList.Add(UnifySurveyDBModel(tempList, newList.Count));
                }

                return newList;
            }
        }

        public List<SurveyDBModelExtended> GetAMemberAggregatedSurveyData(long memberId)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter {ParameterName = "member_id", SqlDbType = SqlDbType.BigInt, Value =  memberId},
                };

                var results = db.Database.SqlQuery<SurveyDBModel>("sp_report_member @member_id", parameters).ToList();

                var groupList = results.GroupBy(m => m.MemberId)
                    .Select(grp => grp.ToList())
                    .ToList();

                int maxItems = 0;
                foreach (var item in groupList)
                {
                    if (item.Count() > maxItems) maxItems = item.Count();
                }

                var newList = new List<SurveyDBModelExtended>();

                for (int i = 0; i < maxItems; i++)
                {
                    var tempList = new List<SurveyDBModel>();
                    foreach (var group in groupList)
                    {
                        if (group.Count > i)
                            tempList.Add(group[i]);
                    }
                    newList.Add(UnifySurveyDBModel(tempList, newList.Count));
                }

                return newList;
            }
        }

        public List<SurveyDBModelExtended> GetGraudatesAggregatedSurveyData()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter {ParameterName = "member_id", SqlDbType = SqlDbType.BigInt, Value =  DBNull.Value},
                };

                var results = db.Database.SqlQuery<SurveyDBModel>("sp_report_member @member_id", parameters).ToList();
                var graduateIds = db.Database.SqlQuery<long>("sp_get_graduate_members").ToList();

                var groupList = results.GroupBy(m => m.MemberId)
                    .Select(grp => grp.ToList())
                    .ToList();

                int maxItems = 0;
                foreach (var group in groupList)
                {
                    bool has = group.Any(x => graduateIds.Contains(x.MemberId));
                    if (!has) continue;

                    if (group.Count() > maxItems) maxItems = group.Count();
                }

                var newList = new List<SurveyDBModelExtended>();
                
                for (int i = 0; i < maxItems; i++)
                {
                    var tempList = new List<SurveyDBModel>();
                    foreach (var group in groupList)
                    {
                        bool has = group.Any(x => graduateIds.Contains(x.MemberId));
                        if (!has) continue;

                        if (group.Count > i)
                            tempList.Add(group[i]);
                    }
                    newList.Add(UnifySurveyDBModel(tempList, newList.Count));
                }

                return newList;
            }
        }

        public List<SurveyDBModelExtended> GetClientsAggregatedSurveyData()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter {ParameterName = "member_id", SqlDbType = SqlDbType.BigInt, Value =  DBNull.Value},
                };

                var results = db.Database.SqlQuery<SurveyDBModel>("sp_report_member @member_id", parameters).ToList();
                var graduateIds = db.Database.SqlQuery<long>("sp_get_graduate_members").ToList();

                var groupList = results.GroupBy(m => m.MemberId)
                    .Select(grp => grp.ToList())
                    .ToList();

                int maxItems = 0;
                foreach (var group in groupList)
                {
                    bool has = group.Any(x => graduateIds.Contains(x.MemberId));
                    if (has) continue;

                    if (group.Count() > maxItems) maxItems = group.Count();
                }

                var newList = new List<SurveyDBModelExtended>();

                for (int i = 0; i < maxItems; i++)
                {
                    var tempList = new List<SurveyDBModel>();
                    foreach (var group in groupList)
                    {
                        bool has = group.Any(x => graduateIds.Contains(x.MemberId));
                        if (has) continue;

                        if (group.Count > i)
                            tempList.Add(group[i]);
                    }
                    newList.Add(UnifySurveyDBModel(tempList, newList.Count));
                }

                return newList;
            }
        }

        public SurveyDBModelExtended UnifySurveyDBModel(List<SurveyDBModel> items, int count)
        {
            long ftEmps = 0;
            long ptEmps = 0;
            decimal gross = 0;
            decimal revenue = 0;
            decimal grant = 0;
            decimal debt = 0;
            decimal equity = 0;
            long totalMembers = items.Count;
            foreach (var item in items)
            {
                ftEmps += item.ftEmplyees;
                ptEmps += item.ptEmployees;
                gross += item.gross;
                revenue += item.revenue;
                grant += item.GrantAmout;
                debt += item.DebtAmount;
                equity += item.EquityAmount;
            }

            return new SurveyDBModelExtended
            {
                SurveyYear = items[0].SurveyYear,
                Header = count == 0 ? "Base" : "Year " + count,
                numberOfMember = totalMembers,
                ftEmplyees = ftEmps,
                ptEmployees = ptEmps,
                gross = gross,
                revenue = revenue,
                GrantAmout = grant,
                DebtAmount = debt,
                EquityAmount = equity
            };
        }
    }
}
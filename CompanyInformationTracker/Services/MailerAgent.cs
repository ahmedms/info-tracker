﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompanyInformationTracker.Services
{
    public class MailerAgent
    {
        public static void send(string to, string subject, string body, bool isHtml)
        {
            GMailService.GmailUsername = "databaseprojectcsci5250@gmail.com";
            GMailService.GmailPassword = "csci5250";

            GMailService mailer = new GMailService();
            mailer.ToEmail = to;
            mailer.Subject = subject;
            mailer.Body = body;
            mailer.IsHtml = isHtml;
            mailer.Send();
        }
    }
}